# README #

### What is this repository for? ###

* SUMMER (Shiny Utility for Metabolomics and Multiomics Exploratory Research) is a shiny application for one-step analysis of multiomics datasets. 
* SUMMER has several modules to perform Principle Component Analysis, Differential Expression Analysis, Pathway Analysis, and Network Analysis.
* SUMMER was developed at Salk IGC. Please email Ling <lhuang@salk.edu> or Max <mshokhirev@salk.edu> if you have any question.

### How do I get set up? ###

* SUMMER is available at http://summer.salk.edu (recommended)

* If you still prefer to run it locally, it may take some extra steps and configurations. SUMMER has been tested under R 3.4.3 and R 3.3.3. SUMMER is not supposed to work in Windows.

* SUMMER uses some R packages that need to be compiled. Please manually install them if the automatic installation fails.
	* [shiny](https://cran.r-project.org/web/packages/shiny/index.html)
	* [limma](https://www.bioconductor.org/packages/release/bioc/html/limma.html)
	* [visNetwork](https://cran.r-project.org/web/packages/visNetwork/index.html)
	* [igraph](https://cran.r-project.org/web/packages/igraph/index.html)
	* [WebGestaltR](https://cran.r-project.org/web/packages/WebGestaltR/index.html)
	* [DT](https://cran.r-project.org/web/packages/DT/index.html)
	* [ggplot2](https://cran.r-project.org/web/packages/ggplot2/index.html)
	* [plotly](https://cran.r-project.org/web/packages/plotly/index.html)
	* [datatable](https://cran.r-project.org/web/packages/data.table/index.html)
	* [dplyr](https://cran.r-project.org/web/packages/dplyr/index.html)
	* [genefilter](https://bioconductor.org/packages/release/bioc/html/genefilter.html)
	

* To install and use SUMMER as an app on your laptop:
	
	1) `git clone https://bitbucket.org/salkigc/summer.git`
	
	2) `cd summer`

	3) Open `R`
	
	4) `>install.packages("shiny")`
	
	5) `>library("shiny")`

	6) `>runApp(".")` 	
	
	7) SUMMER will try to automatically install dependent packages (permissions required)

	8) SUMMER will appear in an interactive browser window for use
	
### How does SUMMER work? ###

* SUMMER expects Mass Spec intensities as input for metabolomics and proteomics data, microarray fluorescent intensities or FPKM/TPM for transcriptomics data. Raw intensity can be written in the format of "1000" or "1,000".
* At the first step, SUMMER tries to map metabolites to KEGG (Kanehisa et al., 2019) identifiers and gene/protein to entrez gene IDs. It is required to have at least 10 valid mapped IDs to continue. Duplicated IDs and NA in IDs are not allowed. Data row is discarded when there are more than half values are missing.
* Then, missing values are imputed by the minimum value across samples. Intensities are then quantile-normalized. 
* Next, transcripts or protein abundances are aggregated to calculate the catalytic enzyme activity. In brief, the enzyme subunit with least abundance is used and the sum of all isoenzyme abundances are used to represent the enzyme activity. 
* All data is then log2-transformed for downstream analysis. A pseudo count of 1 is added before log-transformation.
* Principal Component Analysis is used on the top 500 most variable genes/proteins and total metabolites to visualize samples separation.
* SUMMER leverages the LIMMA bioinformatics package (Ritchie et al., 2015) to perform differential expression testing for the metabolites and a bootstrap approach to to identify significantly altered reactions, which integrate metabolites and enzymes data.
* SUMMER performs a functional enrichment analysis by WebGestaltR (Wang et al., 2017) on the DE metabolites and DE reactions to identify over-represented KEGG pathways. 
* SUMMER provides two modes of network graph. In the pathway view of the network graph, all measured metabolites will be shown regardless of their statistical significance. In the global view of the network graph, only DE metabolites and DE reactions plus their associated reactions and measured metabolites will be shown. 
* All data and interactive plots can be downloaded for further validation and exploratory analysis.

